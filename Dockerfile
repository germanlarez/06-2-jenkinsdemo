FROM node:16.16-alpine

RUN mkdir -p /home/nodeApp

COPY app/ /home/nodeApp/

WORKDIR /home/nodeApp

EXPOSE 3000

RUN npm install

CMD ["node","server.js"]