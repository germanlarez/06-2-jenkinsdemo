def versionIncrement() {
    echo "Incrementing app version in package.json file"
    sh "npm version minor"
    def packageJson = readJSON file: 'package.json'
    def version = packageJson.version
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
    echo "New version = ${IMAGE_NAME}"
}

def testApp() {
    echo "Running tests for branch ${BRANCH_NAME} branch"
    echo "Installing modules for testing"
    sh 'npm install'
    echo "Testing if index.html exists"
    sh 'npm run test'
}

def buildDocker() {
    echo "Building the application from groovy script..."
    withCredentials([usernamePassword(credentialsId: 'DockerHub-Repo', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh "docker build -t ${DOCKER_REPO}:${APP_NAME}-${IMAGE_NAME} ."
        sh "echo ${PASSWORD} | docker login -u ${USERNAME} --password-stdin"
        sh "docker push ${DOCKER_REPO}:${APP_NAME}-${IMAGE_NAME}"
    }    
}

def deployApp() {
    echo "Deploying the application from groovy script to linode K8s Cluster..."
    withKubeConfig([credentialsId: 'k8s-credentials', serverUrl: 'https://7293fae4-4c9d-4629-bc82-262d0a2b8e3c.eu-central-2.linodelke.net']) {
                        withCredentials([usernamePassword(credentialsId: 'DockerHub-Repo', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                            sh "kubectl create secret docker-registry docker-registry-key --docker-server=docker.io --docker-username=${USERNAME} --docker-password=${PASSWORD}"
                        }
                        sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
    }
}

def commitBackGit() {
    echo "Committing back new app version to Git repo"
    withCredentials([usernamePassword(credentialsId: 'jenkinsIncrementVerToken', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh 'git config --global user.email jenkins@glarez.com'
        sh 'git config --global user.name jenkins'
        sh 'git status'
        sh 'git branch'
        sh 'git config --list'
        sh "git remote set-url origin https://${USERNAME}:${PASSWORD}@gitlab.com/germanlarez/06-2-jenkinsdemo.git"
        sh 'git add .'
        sh "git commit -m 'Jenkins CI version bump in package.json to version ${IMAGE_NAME}'"
        sh 'git push origin HEAD:master'
        }
}

return this